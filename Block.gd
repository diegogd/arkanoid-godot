extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

export (Texture) var defaultTexture
export (Texture) var breakedTexture
export var strength = 1

func _ready():
	add_to_group("blocks")
	$Sprite.texture = defaultTexture

func hit():
	print ("Bloque tocado")
	strength -= 1
	$Sprite.texture = breakedTexture
	
	if strength < 0:
		queue_free()
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
