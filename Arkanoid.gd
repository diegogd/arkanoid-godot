extends Node

var level = 0
var levels = [
	"res://Levels/Level0.tscn",
	"res://Levels/Level1.tscn",
	"res://Levels/Level2.tscn",
	"res://Levels/Level3.tscn"
];
var activeLevelNode = null

func _ready():
	nextLevel()
	pass

func nextLevel():
	$Pelota.resetPosition()
	var currentLevel = load(levels[level])
	activeLevelNode = currentLevel.instance()
	add_child(activeLevelNode)

func _process(delta):
	if activeLevelNode.get_children().size() == 0:
		level += 1
		nextLevel()
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
