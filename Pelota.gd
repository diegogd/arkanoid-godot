extends Area2D

export var InitialPosition = Vector2(536, 296)
export var Velocity = 400
export var InitialDirection = -45
var Direction = InitialDirection
var hitOnCycle = 0
var moving = false

func resetPosition():
	moving = false
	position = InitialPosition
	Direction = InitialDirection
	randomize()
	if randf() > 0.5:
		Direction -= 90

func _ready():	
	resetPosition()

func _physics_process(delta):
	if Input.is_key_pressed(KEY_SPACE):
		moving = true
	var x = cos(deg2rad(Direction))
	var y = sin(deg2rad(Direction))
	if moving: 
		var newPos = Vector2(x,y) * Velocity * delta
		position.x += newPos.x
		position.y += newPos.y
	else:
		position.x = $"../Player".position.x
	hitOnCycle = 0

func _on_Pelota_body_entered(body):
	var x = cos(deg2rad(Direction))
	var y = sin(deg2rad(Direction))
	# Posición de la celda en la que está la pelota
	var cellPos = body.world_to_map(position*4)
	if (cellPos.y <= 1 || cellPos.y >= 17):
		print ('Colide on floor or ceiling')
		y *= -1
	if (cellPos.x <= 1 || cellPos.x >= 30):
		print ('Colide on walls')
		x *= -1
		
	Direction = rad2deg(atan2(y, x))
	
	if (cellPos.y >= 17):
		resetPosition()

func _on_Pelota_area_entered(area):
	if area.name == 'Player':
		var x = cos(deg2rad(Direction))
		var y = sin(deg2rad(Direction))
		y *= -1		
		Direction = rad2deg(atan2(y, x))
	if area.is_in_group("blocks"):	
		if hitOnCycle == 0:
			area.hit()
			var angleToBlock = get_angle_to(area.position)
	
			var x = cos(deg2rad(Direction))
			var y = sin(deg2rad(Direction))
			
			if abs(cos(angleToBlock)) > abs(sin(angleToBlock)):
				print ("colide to side")
				x *= -1
			else:
				print ("colide to bot/top")
				y *= -1
			
			Direction = rad2deg(atan2(y, x))
		
		hitOnCycle += 1

