extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var VELOCITY = 200
var oldPosition = Vector2()
var movement = 0

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _physics_process(delta):
	oldPosition = position	

	if Input.is_key_pressed(KEY_RIGHT) && get_overlapping_bodies().size() == 0:
		position.x = position.x + (VELOCITY*delta)		
	if Input.is_key_pressed(KEY_LEFT) && get_overlapping_bodies().size() == 0:
		position.x = position.x - (VELOCITY*delta)		


func _on_Player_body_entered(body):
	print ("Entro en contacto con ", body.get_name())
	position = oldPosition
	
	
